// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Vuetify from 'vuetify'
import * as firebase from 'firebase'
const VueGoogleMaps = require('vue2-google-maps')

import App from './App'
import router from './router'
import { store } from './store'
import DateFilter from './filters/date'
import TimeFilter from './filters/time'

import AlertComp from './components/Shared/Alert.vue'
import GoogleMaps from './components/Shared/GoogleMaps.vue'
import EditMeetupDetails from './components/Meetup/Edit/EditMeetupDetailsDialog.vue'
import EditMeetupDate from './components/Meetup/Edit/EditMeetupDate.vue'
import EditMeetupTime from './components/Meetup/Edit/EditMeetupTime.vue'
import RegisterDialog from './components/Meetup/Registration/RegisterDialog.vue'

Vue.use(Vuetify)
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyDuFwXbmJ2JgDODYsnG0r9HH_5FkQ6Q1K0',
    libraries: 'places'
  }
})
Vue.config.productionTip = false

Vue.component('app-alert', AlertComp)
Vue.component('google-map', GoogleMaps)
Vue.component('app-edit-meetup-details', EditMeetupDetails)
Vue.component('app-edit-meetup-date', EditMeetupDate)
Vue.component('app-edit-meetup-time', EditMeetupTime)
Vue.component('app-meetup-register', RegisterDialog)

Vue.filter('date', DateFilter)
Vue.filter('time', TimeFilter)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: {App},
  created () {
    firebase.initializeApp({
      apiKey: 'AIzaSyBMPFQsvCW9opfGTHUglhG4kJ_yYscSDc0',
      authDomain: 'speedup-portfolio-app.firebaseapp.com',
      databaseURL: 'https://speedup-portfolio-app.firebaseio.com',
      projectId: 'speedup-portfolio-app',
      storageBucket: 'gs://speedup-portfolio-app.appspot.com'
    })
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.$store.dispatch('autoSignIn', user)
        this.$store.dispatch('fetchUserData')
      }
    })
    this.$store.dispatch('loadMeetups')
  }
})

