import Vue from 'vue'
import Vuex from 'vuex'

import meetup from './meetups/index'
import user from './users/index'
import shared from './shared/index'

Vue.use(Vuex)

export const store = new Vuex.Store({
  modules: {
    meetup: meetup,
    user: user,
    shared: shared
  }
})
